let RsvpListener = require('./rsvp-listener');
let moment = require('moment');

let numSeconds = 60;
if(process.argv.length > 2)
{
    numSeconds = parseInt(process.argv[2], 10);

    // Basic sanity checking to verify the user didn't do something silly
    if(isNaN(numSeconds) || numSeconds < 0)
    {
        throw new Error(`Invalid number of seconds specified: ${process.argv[2]}`);
    }
}

// Create the response listener and start listening!
let responseListener = new RsvpListener("ws://stream.meetup.com/2/rsvps");
responseListener.listen(numSeconds).then(response =>
{
    // It could be the case that we have less than 3 countries that came through the socket, we add on some empty ones
    //  for formatting purposes. Also, we destructure the array
    let denormedTop3 = response.top3.concat(
        (new Array(3 - response.top3.length)).fill({country: '', count: 0})
    ).reduce((acc, next) => {
        acc.push(next.country);
        acc.push(next.count);
        return acc;
    }, []);

    // Output format: total,future_date,future_url,co_1,co_1_count,co_2,co_2_count,co_3,co_3_count
    console.log([
        response.total,
        response.mostFutureEvent ? moment(response.mostFutureEvent.time).toISOString() : '',
        response.mostFutureEvent ? response.mostFutureEvent.url : ''
    ].concat(denormedTop3).join(','));
}, error => {
    console.error(error);
});
