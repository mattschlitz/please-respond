let ws = require('nodejs-websocket');

/**
 * @class RsvpListener - This class can be used to listen to the Meetup.com RSVP Api and collect some metrics about it
 *  after a certain amount of time
 */
class RsvpListener {
    /**
     * @param wsUrl string - The URL for the websocket endpoint to connect to
     */
    constructor(wsUrl){
        this.wsUrl = wsUrl;
    }

    /**
     * listen(numSeconds) - Listens to the websocket endpoint for a specified number of seconds and returns the
     *  aggregated metrics based on what is received
     *
     * @param numSeconds number - The number of seconds to listen to the websocket for
     * @returns {Promise<object>} A promise that, when resolved, has the following metrics from the streamed data:
     *      total (number) - The total number of messages received
     *      mostFutureEvent ({time: number, url: string}) - The time and url of the event that is happening furthest
     *          into the future that was received
     *      top3 ({country: string, count: number}[]) - The top 3 countries, by total number of events received
     */
    listen(numSeconds){
        let totals = {
            total: 0,
            mostFutureEvent: null,
            eventCountPerCountry: {}
        };
        let connection = ws.connect(this.wsUrl);
        connection.on('text', (text) => {
            // Parse the raw data we're getting
            let input = JSON.parse(text);

            // Collect metrics
            totals.total++;
            if(totals.mostFutureEvent === null || totals.mostFutureEvent.time < input.event.time)
            {
                totals.mostFutureEvent = {
                    time: input.event.time,
                    url: input.event.event_url
                };
            }
            totals.eventCountPerCountry[input.group.group_country] =
                (totals.eventCountPerCountry[input.group.group_country] || 0) + 1;

        });

        connection.on('error', err => {
            // Ideally, "err" would be logged somewhere useful but, for now, we just use it as a way to be a little more
            // fault tolerant
        });

        // Wait for the connection to be established before we start the timer
        connection.on('connect', () => {
            setTimeout(() => {
                // After the time is up, cleanup the connection and the event firing will ultimately resolve the promise
                connection.close();
            }, numSeconds*1000);
        });

        return new Promise((resolve, reject) => {
            // We'll reject the promise if we couldn't make a connection
            connection.on('close', () => {
                if(connection.readyState === 0)
                {
                    reject("Connection could not be established. Please check your network connection.");
                }
                else
                {
                    resolve({
                        total: totals.total,
                        mostFutureEvent: totals.mostFutureEvent,
                        top3: RsvpListener.processTopCountries(totals.eventCountPerCountry)
                    });
                }
            });
        })
    }

    /**
     * processTopCountries(countriesWithTotals, maxAllowed) - A helper function for converting the top countries from a
     *  hash table (keyed by countries, whose values are the counts for the respective country) into an array of
     *  country objects, ordered by the counts and truncated after the maxAllowed specified
     *
     * @param countriesWithTotals object - The hash table with countries/counts
     * @param maxAllowed number - The maximum number of allowed top countries (default: 3)
     * @returns {{country: string, count: number}[]} A list of countries and counts. If multiple countries have the
     *  same count, then the order the hash table was processed will dictate which country will come first and,
     *  therefore, which countries may be truncated off the list
     */
    static processTopCountries(countriesWithTotals, maxAllowed = 3)
    {
        let topCountries = [];
        for(let country in countriesWithTotals)
        {
            let count = countriesWithTotals[country];

            // We want to treat the topCountries list like a priority queue. This is a simple way to let things fall off
            //  when we insert a new value
            let inserted = false;
            for(let i = 0; i < topCountries.length; i++)
            {
                if(inserted = topCountries[i].count < count)
                {
                    for(let j = topCountries.length - 1; j > i; j--)
                    {
                        topCountries[j] = topCountries[j - 1];
                    }
                    topCountries[i] = {country, count};
                    break;
                }
            }

            // If we didn't insert the value and we haven't used up all the spaces, just tack it onto the end
            if(!inserted && topCountries.length < maxAllowed)
            {
                topCountries.push({country, count});
            }
        }
        return topCountries;
    }
}

module.exports = RsvpListener;
